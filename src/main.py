import argparse
import random
import tabulate
from sklearn.metrics import classification_report
from nltk.corpus import genesis
from google.cloud import translate
from pprint import pprint


sentences = [
    'i love my grandma',
    'J\'aime ma grand-mère',
    'ik houd van mijn oma',
    'Ich habe meine Oma lieb',
    'Amo a mi abuela',
    'Jag älskar min mormor'
]


def detect_language(text):
    """Detects the text's language."""
    return translate.Client().detect_language(text)


def print_detect_res(text):
    lid_to_l = {}
    for lang in translate.Client().get_languages():
        lid_to_l[lang['language']] = lang['name']

    res = detect_language(text)
    res['text'] = text
    res['language'] = lid_to_l[res['language']]
    print('Text: {text}\nLanguage: {language}\nConfidence: {confidence:0.4f}'.format(**res))


def perf_report():
    actuals = []
    to_predict = []

    for lng_id, lng_src in zip(['en', 'fr', 'de', 'pt'],
                               ['english-web.txt', 'french.txt', 'german.txt', 'portuguese.txt']):
        sents = [' '.join(x) for x in random.sample(list(genesis.sents(lng_src)), 100)]
        to_predict.extend(sents)
        actuals.extend([lng_id] * len(sents))

    tbl_data = random.sample([[actuals[i], to_predict[i]] for i in range(len(actuals))], 10)
    print(tabulate.tabulate(tbl_data, headers=('lang', 'sentence')))

    predicted = [detect_language(t)['language'] for t in to_predict]

    print(classification_report(actuals, predicted))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    subparsers = parser.add_subparsers(dest='command')

    detect_langage_parser = subparsers.add_parser(
        'demo', help=detect_language.__doc__)
    detect_langage_parser.add_argument('text')

    subparsers.add_parser('performance', help=perf_report.__doc__)

    subparsers.add_parser('languages')
    subparsers.add_parser('demo_canned')

    args = parser.parse_args()

    if args.command == 'demo':
        print_detect_res(args.text)
    elif args.command == 'performance':
        perf_report()
    elif args.command == 'languages':
        pprint(translate.Client().get_languages())
    elif args.command == 'demo_canned':
        for sent in sentences:
            print_detect_res(sent)
            print()